ITEM.ClassName = "item_trash"
--ITEM.Base = "base_trash"

ITEM.iconModel = "models/props/cs_office/cardboard_box03.mdl"
ITEM.worldModel = "models/props/cs_office/cardboard_box03.mdl"
ITEM.printName = "Trash"
ITEM.weight = 1
ITEM.stackCount = 2
ITEM.category = "Trash"
ITEM.description = {
	"A Piece of trash.",
	"Holds no real value but maybe you can craft something from it.",
}
function ITEM:use( ply )
end