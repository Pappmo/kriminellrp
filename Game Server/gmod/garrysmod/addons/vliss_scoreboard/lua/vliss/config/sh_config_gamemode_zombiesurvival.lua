-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.4.0
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ ZOMBIE SURVIVAL MODE SETTINGS ]
-----------------------------------------------------------------
-- These settings are for the ZOMBIE SURVIVAL GAMEMODE ONLY.
-- If you are using Vliss on another gamemode, then you should 
-- set Vliss.ZombieSurvival.Enabled = false
-----------------------------------------------------------------

Vliss.ZombieSurvival.Enabled = false -- Set this to true if Murder is your gamemode.

-----------------------------------------------------------------
-- [ TEAM ASSIGNMENTS - ADVANCED DEVELOPERS ONLY ]
-----------------------------------------------------------------
-- Team assignments for the gamemode. You should not mess with 
-- these unless you know what you're doing.
-----------------------------------------------------------------
-- Vliss.ZombieSurvival.TeamHuman 		4
-- Vliss.ZombieSurvival.TeamUndead 		3
-- Vliss.ZombieSurvival.TeamSpectators 	1002
-----------------------------------------------------------------

Vliss.ZombieSurvival.TeamHuman = 4
Vliss.ZombieSurvival.TeamUndead = 3
Vliss.ZombieSurvival.TeamSpectators = 1002

-----------------------------------------------------------------
-- [ OTHER SETTINGS ]
-----------------------------------------------------------------

Vliss.ZombieSurvival.TeamHumanTitle = "Humans"
Vliss.ZombieSurvival.TeamUndeadTitle = "Undead"
Vliss.ZombieSurvival.SpectatorsTitle = "Spectators"
