-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.4.0
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ PLAYER ACTION COMMANDS ]
-----------------------------------------------------------------
-- When you click on a player within the scoreboard, actions 
-- will be displayed. The list below defines what other staff 
-- are allowed to do to a player. It is based upon what admin mod
-- you have installed. New ones are always being added for support
-- so if it's not here - it's in the works.
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ PLAYER ACTION COMMANDS  - ULX ]
-----------------------------------------------------------------

Vliss.Commands.Ulx = {
    {
        name = "Bring",
        cmd = "bring"
    },
    {
        name = "Return",
        cmd = "return"
    },
    {
        name = "Jail",
        cmd = "jail"
    },
    {
        name = "Ragdoll",
        cmd = "ragdoll"
    },
    {
        name = "Kick",
        cmd = "kick"
    },
    {
        name = "Ignite",
        cmd = "ignite"
    },
    {
        name = "Whip",
        cmd = "whip"
    },
    {
        name = "Maul",
        cmd = "maul"
    },
    {
        name = "Strip",
        cmd = "strip"
    },
    {
        name = "Gag",
        cmd = "gag"
    },
    {
        name = "Mute",
        cmd = "mute"
    }
}

-----------------------------------------------------------------
-- [ PLAYER ACTION COMMANDS  - EVOLVE ]
-----------------------------------------------------------------

Vliss.Commands.Evolve = {
    {
        name = "Kick",
        cmd = "kick"
    }
}

-----------------------------------------------------------------
-- [ PLAYER ACTION COMMANDS  - EXSTO ]
-----------------------------------------------------------------

Vliss.Commands.Exsto = {
    {
        name = "Kick",
        cmd = "kick"
    }
}

-----------------------------------------------------------------
-- [ PLAYER ACTION COMMANDS  - FADMIN (DARKRP) ]
-----------------------------------------------------------------

Vliss.Commands.FAdmin = {
    {
        name = "Kick",
        cmd = "kick"
    }
}