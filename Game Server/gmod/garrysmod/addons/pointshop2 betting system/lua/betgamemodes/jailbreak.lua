if JB and JB.ThisRound then include('betgamemodes/excljailbreak.lua') return end
--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for the Jailbreak gamemode by my_hat_stinks. For Excl's Jailbreak see excljailbreak.lua

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_JAILOR
BETTING.SecondTeam = TEAM_PRISONER
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5

local function PlayerCanBetJailbreak(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:IsActive() then return false,"Players must be dead or spectating to bet." end
	if GetRoundState() != ROUND_ACTIVE and GetRoundState() != ROUND_PREP then return false,"You cant bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetJailbreak",PlayerCanBetJailbreak)

--We need to override SetRoundState on both SERVER and CLIENT because no hooks are called
local OldSetRoundState = SetRoundState
function SetRoundState(state)
	OldSetRoundState(state)
	if (state == ROUND_PREP) then BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30 end
end
--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
	local OldSendEndRoundInfo = gmod.GetGamemode().SendEndRoundInfo
	local function JailbreakEndRoundBets(gm,endtype)
		OldSendEndRoundInfo(gm,endtype)
		if not endtype then return end
		if endtype == END_JAILOR then endtype = 3 end
		if (endtype == END_TIME) then BETTING.FinishBets(endtype, true)
		else BETTING.FinishBets(endtype) end
	end
	gmod.GetGamemode().SendEndRoundInfo = JailbreakEndRoundBets
end