/*---------------------------------------------------------
WinnerPanel
---------------------------------------------------------*/
local WinnerPanel = {}

function WinnerPanel:Init()
	self:SetDrawBackground(false)
	//self:SetDrawBorder(false)
	//self:SetStretchToFit(false)
	self:SetSize(ScrW() / 2.5, 50)
	self.BackColor = BETTING.Theme.ControlColor
	self.FlashColor = self.BackColor
	self.TextColor = Color(255, 255, 255, 150 )
	self.PrimaryBarColor = Color(0, 204, 68)
	self.WinningColor = Color(0, 204, 68)
	self.LosingColor = Color(255, 77, 77)
	self.Hovering = false
	
	self.HeaderLbl = vgui.Create("DLabel", self)
	self.HeaderLbl:SetFont("OpenSans24Font")
	
	self.Avatar = vgui.Create("AvatarImage",self)
	self.Avatar:SetSize( 50, 50 )
end

function WinnerPanel:SetData(ply,amount,win)
	self.Avatar:SetPlayer(ply, 64)
	self.HeaderLbl:SetText(string.format("%s %s the most %s in this round.\nThey %s a total of %s %s. %s!",
	ply:Nick(), win and "wins" or "loses",BETTING.GetPointsName(),win and "win" or "lose",BETTING.FormatNumber(amount),
	BETTING.GetPointsName(), win and "Well Done" or "Better luck next time"))
	self.HeaderLbl:SizeToContents()
	
	self.PrimaryBarColor = win and self.WinningColor or self.LosingColor
end

function WinnerPanel:PerformLayout()
	self.Avatar:SetPos(5,10)
	
	self.HeaderLbl:SetPos(5,10)
	self.HeaderLbl:MoveRightOf(self.Avatar, 10)
end

function WinnerPanel:Paint()
	surface.SetDrawColor(self.BackColor)
	surface.DrawRect( 0, 0, self:GetWide(), self:GetTall())
	
	//Primary tab line
	surface.SetDrawColor(self.PrimaryBarColor)
	surface.DrawRect( 0, 0, self:GetWide(), 5)
end

function WinnerPanel:PaintOver()
end

function WinnerPanel:OnCursorEntered() 
end

function WinnerPanel:OnCursorExited() 
end

function WinnerPanel:ColorWithCurrentAlpha(c)
	local r,g,b = c.r,c.g,c.b
	return Color(r,g,b,self.CurrentFlashAlpha)
end
derma.DefineControl("WinnerPanel", "Betting highest winner or loser panel", WinnerPanel, "DPanel")

/*---------------------------------------------------------
End of WinnerPanel
---------------------------------------------------------*/