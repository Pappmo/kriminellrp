--Pointshop Betting Main Client Dist
if BETTING then BETTING = BETTING
else BETTING = {} end
BETTING.BetTotal = BETTING.BetTotal or 0
BETTING.ActiveBet = false
include('cl_bettingfonts.lua')
include('sh_bettingconfig.lua')
include('sh_pointshopbetting.lua')
--Include panels
include('panels/cl_betpanel.lua')
include('panels/cl_betbutton.lua')
include('panels/cl_winnerpanel.lua')

function BETTING.OpenBetScreen( settings )
	if not LocalPlayer() or not (PS or Pointshop2) then return end
	if not BETTING.IsTeamValid(BETTING.FirstTeam) or not BETTING.IsTeamValid(BETTING.SecondTeam) then
		ErrorNoHalt("[POINTSHOP BETTING] Error: Betting teams are not configured.")
		return
	end
	if not BETTING.PlayerIsOnFirstTeam(LocalPlayer()) and not BETTING.PlayerIsOnSecondTeam(LocalPlayer()) then return end
	BETTING.MainWindowOpen = true
	if !BettingMainWindow then
		
		BettingMainWindow = vgui.Create("BetPanel")
		//BettingMainWindow:ParentToHUD()
		BettingMainWindow:SetSize(300,325)
		BettingMainWindow:SetPos(BETTING.Settings.ShowOnRight and ScrW() + 1 or -BettingMainWindow:GetWide(), (ScrH() - BettingMainWindow:GetTall()) / 2)
		BettingMainWindow:MoveTo(BETTING.Settings.ShowOnRight and ScrW() - 305 or 5, (ScrH() - BettingMainWindow:GetTall()) / 2, 0.5, 0, 1, function() BettingMainWindow:AlphaTo(200,0.5,0) end)
		
		//Bet total
		BettingMainWindow:SetTotalBets(BETTING.BetTotal,BETTING.GetPointsName())
		//Bet results?
		if settings and settings.BetWon then
			BettingMainWindow:SetWinningBet(settings.BetOn, settings.BetWinnings,BETTING.Settings.ShowBetResultsTime)
			return
		elseif BettingMainWindow and settings and settings.BetLost then
			BettingMainWindow:SetLosingBet(settings.BetOn, settings.BetAmount,BETTING.Settings.ShowBetResultsTime)
			return
		end
		//Calculate team with most alive players
		local firsteam,secondteam = 0,0
		for k,v in pairs(player.GetAll()) do
			if v:Alive() then
				if BETTING.PlayerIsOnFirstTeam(v) then firsteam  = (firsteam + 1)
				elseif BETTING.PlayerIsOnSecondTeam(v) then secondteam = (secondteam + 1) end
			end
		end
		
		if BETTING.Settings.OnlyAllowBetsSelf or BETTING.Settings.AllowBetsOnOthers then
			BettingMainWindow:SetData(BETTING.GetPlayerTeam(LocalPlayer()),nil,BETTING.Settings.BetMultiplier,BETTING.Settings.AllowBetsOnOthers)
		elseif secondteam > firsteam then
			BettingMainWindow:SetData(BETTING.GetTeamID(BETTING.SecondTeam),BETTING.GetTeamID(BETTING.FirstTeam),BETTING.Settings.BetMultiplier)
		else BettingMainWindow:SetData(BETTING.GetTeamID(BETTING.FirstTeam),BETTING.GetTeamID(BETTING.SecondTeam),BETTING.Settings.BetMultiplier) end
		BettingMainWindow.DoClick = function()
			if BettingMainWindow:GetActiveBet() then return end
			BETTING.BetAmountDialog(string.format("Place a bet on %s",BETTING.Settings.AllowBetsOnOthers and "Any Player" or BETTING.Settings.OnlyAllowBetsSelf and "Yourself" or BETTING.GetTeamName(BettingMainWindow:GetPrimaryTeam())),
			string.format("Choose the amount you want to bet. If you win you will get x%i %s",BETTING.Settings.BetMultiplier,BETTING.GetPointsName()), 
			function(amt, betply, allin) RunConsoleCommand("PSBetting_NewBet", BettingMainWindow:GetPrimaryTeam(), amt, allin and 1 or 0, betply and betply:EntIndex()) end,BETTING.GetTeamColor(BettingMainWindow:GetPrimaryTeam()))
		end
		BettingMainWindow.SecondaryBtn.DoClick = function()
			if BettingMainWindow:GetActiveBet() then return end
			BETTING.BetAmountDialog(string.format("Place a bet on %s",BETTING.Settings.AllowBetsOnOthers and "Any Player" or BETTING.Settings.OnlyAllowBetsSelf and "Yourself" or BETTING.GetTeamName(BettingMainWindow:GetSecondaryTeam())),
			string.format("Choose the amount you want to bet. If you win you will get x%i %s",BETTING.Settings.BetMultiplier,BETTING.GetPointsName()),
			function(amt, betply, allin) RunConsoleCommand("PSBetting_NewBet", BettingMainWindow:GetSecondaryTeam(), amt, allin and 1 or 0, betply and betply:EntIndex()) end,BETTING.GetTeamColor(BettingMainWindow:GetSecondaryTeam()))
		end
		
		if BETTING.Settings.ShowFKeyShowCursorMessage and BETTING.Settings.FKeyShowCursor and BETTING.Settings.FKeyShowCursor != "" then
			BettingMainWindow:SetFKeyMessage(BETTING.Settings.FKeyShowCursor, 5)
		end
		
		if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
			BettingMainWindow:SetTimer(BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
		end
	elseif BettingMainWindow and settings and settings.BetWon then
		BettingMainWindow:SetWinningBet(settings.BetOn, settings.BetWinnings)
	elseif BettingMainWindow and settings and settings.BetLost then
		BettingMainWindow:SetLosingBet(settings.BetOn, settings.BetAmount)
	else
		BETTING.CloseVoteScreen()
	end
end
concommand.Add("betting_open",BETTING.OpenBetScreen)
//usermessage.Hook("VOTING_Open", BETTING.OpenVoteScreen)

function BETTING.BetPlaced(msg)
	if not BettingMainWindow then return end
	local beton = msg:ReadLong()
	local amount = msg:ReadLong()
	local plybeton = msg:ReadEntity()
	BETTING.ActiveBet = true
	BettingMainWindow:SetActiveBet(beton, amount, plybeton)
	
	BETTING.PlaySound(BETTING.Settings.BetPlacedSound, 0.5)
end
usermessage.Hook("PSBetting_BetPlaced", BETTING.BetPlaced)

function BETTING.BetWon(msg)
	local beton = msg:ReadLong()
	local winnings = msg:ReadLong()
	local settings = {}
	settings.BetWon = true
	settings.BetOn = beton
	settings.BetWinnings = winnings
	BETTING.ActiveBet = false
	BETTING.OpenBetScreen( settings )
	
	BettingMainWindow:MoveTo(BETTING.Settings.ShowOnRight and ScrW() + 1 or -BettingMainWindow:GetWide(), (ScrH() - BettingMainWindow:GetTall()) / 2, 0.5, BETTING.Settings.ShowBetResultsTime, 1, function() BETTING.CloseVoteScreen() end)
	
	if BETTING.Settings.WinningBetMusic and #BETTING.Settings.WinningBetMusic > 1 then
		BETTING.PlaySound(table.Random(BETTING.Settings.WinningBetMusic), 0.5)
	end
end
usermessage.Hook("PSBetting_BetWon", BETTING.BetWon)

function BETTING.BetLost(msg)
	local beton = msg:ReadLong()
	local amount = msg:ReadLong()
	local settings = {}
	settings.BetLost = true
	settings.BetOn = beton
	settings.BetAmount = amount
	BETTING.ActiveBet = false
	BETTING.OpenBetScreen( settings )
	
	BettingMainWindow:MoveTo(BETTING.Settings.ShowOnRight and ScrW() + 1 or -BettingMainWindow:GetWide(), (ScrH() - BettingMainWindow:GetTall()) / 2, 0.5, BETTING.Settings.ShowBetResultsTime, 1, function() BETTING.CloseVoteScreen() end)
	
	if BETTING.Settings.LosingBetMusic and #BETTING.Settings.LosingBetMusic > 1 then
		BETTING.PlaySound(table.Random(BETTING.Settings.LosingBetMusic), 0.5)
	end
end
usermessage.Hook("PSBetting_BetLost", BETTING.BetLost)

function BETTING.UpdateBetTotal()
	local total = net.ReadInt(32)
	BETTING.BetTotal = total
	if not tonumber(total) then return end
	if BettingMainWindow then BettingMainWindow:SetTotalBets(BETTING.FormatNumber(total),BETTING.GetPointsName()) end
end
net.Receive("PSBetting_UpdateTotal",BETTING.UpdateBetTotal)

function BETTING.ResetBetTotal()
	BETTING.BetTotal = 0
end
usermessage.Hook("PSBetting_ResetBetTotal", BETTING.ResetBetTotal)

function BETTING.ShowHighestWinnerOrLoser(msg)
	local ply = msg:ReadEntity()
	local winorlose = msg:ReadBool()
	local amount = msg:ReadLong()
	if not IsValid(ply) or not tonumber(amount) then return end

	local WinnerPanel = vgui.Create("WinnerPanel")
	WinnerPanel:SetZPos(999)
	WinnerPanel:SetAlpha(150)
	WinnerPanel:SetSize(ScrW() / 2.5, 65)
	WinnerPanel:SetPos((ScrW() - WinnerPanel:GetWide()) / 2, -WinnerPanel:GetTall())
	WinnerPanel:MoveTo((ScrW() - WinnerPanel:GetWide()) / 2,10, 0.5, 0, 1, function() WinnerPanel:AlphaTo(225,0.5,0) end)
	
	WinnerPanel:SetData(ply,amount,winorlose)
	
	WinnerPanel:AlphaTo(0,1.5,BETTING.Settings.ShowHighestWinnerOrLoserTime,function() WinnerPanel:Remove() WinnerPanel = nil end)
	
end
usermessage.Hook("PSBetting_Highest",BETTING.ShowHighestWinnerOrLoser)

BETTING.NextThink = CurTime()
function BETTING.PointshopBettingThink()
	if CurTime() >= BETTING.NextThink then
		BETTING.NextThink = CurTime() + 2
		local showui = hook.Call("PlayerCanBet", nil, LocalPlayer())
		if showui and !BettingMainWindow and not (BETTING.ActiveBet and !LocalPlayer():Alive() and BETTING.Settings.HideBetScreenWhilstDead) then BETTING.OpenBetScreen()
		elseif not showui and BettingMainWindow and not BettingMainWindow:GetActiveBet() then BETTING.CloseVoteScreen()
		elseif BettingMainWindow and not BettingMainWindow:IsShowingBetResult() and (!LocalPlayer():Alive()) and BETTING.Settings.HideBetScreenWhilstDead and BETTING.ActiveBet then BETTING.CloseVoteScreen()
		elseif not showui and BettingMainWindow and tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
		and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 and BETTING.Settings.HideBetScreenWhilstAlive and BettingMainWindow:GetActiveBet() then
			if LocalPlayer():Alive() and BettingMainWindow:GetAlpha() > 0 and not BettingMainWindow:IsShowingBetResult() then BettingMainWindow:AlphaTo(0,1.5,0)
			elseif ((!LocalPlayer():Alive()) or BettingMainWindow:IsShowingBetResult()) and BettingMainWindow:GetAlpha() < 1 and not BettingMainWindow:GetHiding() then BettingMainWindow:AlphaTo(200,0.5,0) end
		end
		
		if BETTING.Settings.HideWhenChatBoxOpen and BettingMainWindow and BettingMainWindow:GetHiding() and !LocalPlayer():IsTyping() then
			BettingMainWindow:SetHiding(false)
		end
	end
end
hook.Add("Think","BETTING_PointshopBettingThink",BETTING.PointshopBettingThink)

if BETTING.Settings.HideWhenChatBoxOpen then
	hook.Add("StartChat","BETTING_HideWhenChatBoxOpen",function()
		if BETTING.Settings.HideWhenChatBoxOpen and BettingMainWindow and BettingMainWindow:GetAlpha() > 1 then
			BettingMainWindow:SetHiding(true)
		end
	end)
end

function BETTING.CloseVoteScreen()
	if BettingMainWindow then
		BettingMainWindow:AlphaTo(0,1,0, function()
			BettingMainWindow:Remove()
			BettingMainWindow = nil
		end)
	end
	BETTING.MainWindowOpen = false
end

function BETTING.ShowChatNotice(msg, text)
	local text = msg and msg:ReadString() or text or "No message."
	chat.AddText(BETTING.Theme.NoticePrefixColor, BETTING.Settings.NoticePrefix .. " ", BETTING.Theme.NoticeTextColor, text )
end
usermessage.Hook("BETTING_PointshopBettingNotice", BETTING.ShowChatNotice)

function BETTING.InitGamemodeScript()
	if file.Exists(string.format("betgamemodes/%s.lua",gmod.GetGamemode().FolderName),"LUA") then
		include(string.format("betgamemodes/%s.lua",gmod.GetGamemode().FolderName))
	elseif gmod.GetGamemode().BaseClass and file.Exists(string.format("betgamemodes/%s.lua",gmod.GetGamemode().BaseClass.FolderName),"LUA") then
		include(string.format("betgamemodes/%s.lua",gmod.GetGamemode().BaseClass.FolderName))
	else print("[POINTSHOP BETTING] Warning: No gamemode script found. Developers must call pointshop betting functions manually.") end
end
hook.Add("InitPostEntity","BETTING_InitGamemodeScript",BETTING.InitGamemodeScript)

--BindKey
local Binds = {
	["gm_showhelp"] = "F1",
	["gm_showteam"] = "F2",
	["gm_showspare1"] = "F3",
	["gm_showspare2"] = "F4"
}

local ShowCursor
function BETTING.PlayerBindPress( ply, bind, down )
	local bnd = string.match(string.lower(bind), "gm_[a-z]+[12]?")
	if bnd and Binds[bnd] and Binds[bnd] == BETTING.Settings.FKeyShowCursor then
		local settings = {}
			ShowCursor = !ShowCursor
			gui.EnableScreenClicker(ShowCursor)
	end
end
hook.Add("PlayerBindPress","BETTING_PlayerBindPress",BETTING.PlayerBindPress)

local PlaceBetPanelHeight = 350
function BETTING.BetAmountDialog(title, text, func, tablinecolor, timeout)
	if not title or not func then return end
	local IsMaxBet = BETTING.Settings.MaximumBet and BETTING.Settings.MaximumBet > 0
		local PlaceBetPanel = vgui.Create( "DFrame" )
		PlaceBetPanel:SetSize( ScrW(), PlaceBetPanelHeight )
		PlaceBetPanel:Center()
		PlaceBetPanel:SetDraggable( false )
		PlaceBetPanel:ShowCloseButton( false )
		PlaceBetPanel:SetTitle( "" )
		PlaceBetPanel:SetBackgroundBlur( true )	
		PlaceBetPanel:SetDrawOnTop( true )
		PlaceBetPanel.Paint = function()
		Derma_DrawBackgroundBlur(PlaceBetPanel)
		surface.SetDrawColor(BETTING.Theme.ControlColor)
		
		surface.DrawRect(0, 0, PlaceBetPanel:GetWide(), PlaceBetPanel:GetTall())
		
		//Paint line
		surface.SetDrawColor(tablinecolor or BETTING.Theme.ControlColor)
		surface.DrawRect(0, 0, PlaceBetPanel:GetWide(), 5)
		end
	
	local Title = vgui.Create("DLabel",PlaceBetPanel)
	Title:SetPos(50, 10)
	Title:SetFont("Bebas70Font")
	Title:SetText(title)
	Title:SizeToContents()
	
	local QuestionText = vgui.Create("DLabel",PlaceBetPanel)
	QuestionText:SetPos(50, 85)
	QuestionText:SetFont("OpenSans24Font")
	QuestionText:SetText(text or "Please enter your input:")
	QuestionText:SizeToContents()
	
	local BetWarning = vgui.Create("DLabel",PlaceBetPanel)
	BetWarning:SetPos(50, 180)
	BetWarning:SetFont("OpenSans24Font")
	BetWarning:SetTextColor(Color(255,77,77))
	
	local ComboBox
	if BETTING.Settings.AllowBetsOnOthers then
		ComboBox = vgui.Create("DComboBox", PlaceBetPanel)
		ComboBox:SetValue("Select A Player")
		ComboBox:SetFont("Bebas40Font")
		ComboBox:SetPos(50, 115)
		ComboBox:SetSize(450, 60)
		local select
		for k,ply in pairs(player.GetAll()) do
			if ply:Alive() then 
				local idx = ComboBox:AddChoice(ply:Nick(), ply)
				if LocalPlayer() == ply then select = idx end
			end
		end
		ComboBox.OnSelect = function( s, idx, val, data )
			if data then PlaceBetPanel.Player = data end
		end
		if select then ComboBox:ChooseOptionID(select) end
	end
	
	local NumberWang = vgui.Create("DNumberWang", PlaceBetPanel)
	NumberWang:SetTextColor( Color(0, 0, 0, 255) )
	NumberWang:SetValue(BETTING.Settings.MinimumBet or 1)
	NumberWang:SetFont("Bebas40Font")
	NumberWang:SetPos(50, 115)
	if ComboBox then NumberWang:MoveRightOf(ComboBox, 10) end
	NumberWang:SetSize(200, 60)
	local points = LocalPlayer() 

	local maxbet = (BETTING.GetPoints(LocalPlayer()) <= BETTING.Settings.MaximumBet) and BETTING.GetPoints(LocalPlayer()) or BETTING.Settings.MaximumBet
	NumberWang:SetMinMax(BETTING.Settings.MinimumBet,maxbet)
	NumberWang.Think = function()
		local betvalue = NumberWang:GetValue()
		if not tonumber(betvalue) then BetWarning:SetVisible(true) BetWarning:SetText("Invalid bet.") BetWarning:SizeToContents()
		elseif (betvalue < BETTING.Settings.MinimumBet) then BetWarning:SetVisible(true) BetWarning:SizeToContents() BetWarning:SetText(string.format("Minimum bet value is %s",BETTING.FormatNumber(BETTING.Settings.MinimumBet)))
		elseif IsMaxBet and (betvalue > BETTING.Settings.MaximumBet) then BetWarning:SetVisible(true) BetWarning:SizeToContents() BetWarning:SetText(string.format("Maximum bet value is %s",BETTING.FormatNumber(BETTING.Settings.MaximumBet)))
		else BetWarning:SetVisible(false) end
	end
	
	local WinningAmount = vgui.Create("DLabel",PlaceBetPanel)
	WinningAmount:SetPos(50, 120)
	WinningAmount:MoveRightOf(NumberWang, 10)
	WinningAmount:SetFont("OpenSans24Font")
	WinningAmount.Think = function()
		local betvalue = NumberWang:GetValue()
		if not tonumber(betvalue) or (betvalue < BETTING.Settings.MinimumBet) or (IsMaxBet and (betvalue > BETTING.Settings.MaximumBet)) then return end
		local winnings = (NumberWang:GetValue() * BETTING.Settings.BetMultiplier)
		WinningAmount:SetText(string.format("%s %s if you win",BETTING.FormatNumber(winnings),BETTING.GetPointsName()))
		WinningAmount:SizeToContents()
	end
	
	local CurrentPoints = vgui.Create("DLabel",PlaceBetPanel)
	CurrentPoints:SetPos(50, 142)
	CurrentPoints:MoveRightOf(NumberWang, 10)
	CurrentPoints:SetFont("OpenSans24Font")
	CurrentPoints:SetText(string.format("You have %s %s available",BETTING.FormatNumber(BETTING.GetPoints(LocalPlayer())),BETTING.GetPointsName()))
	CurrentPoints:SizeToContents()
	
	local ButtonList = vgui.Create( "DPanelList", PlaceBetPanel )
	ButtonList:SetPos(65, PlaceBetPanel:GetTall() - 85)
	ButtonList:SetSize(PlaceBetPanel:GetWide() - 100, 70)
	ButtonList:SetPadding( 0 )
	ButtonList:SetSpacing( 5 )
	ButtonList:SetAutoSize( false )
	ButtonList:SetNoSizing( false )
	ButtonList:EnableHorizontal( true )
	ButtonList:EnableVerticalScrollbar( false )
	ButtonList.Paint = function() end
		
	local SmartScroller = vgui.Create( "DHorizontalScroller", ButtonList )
	SmartScroller:SetOverlap( -5 )
	SmartScroller:Dock(TOP)
	SmartScroller:SetTall(70)

	local buttons = {}
	table.insert(buttons, {text = "Place Bet", color = Color(51,255,51), func = func})
	if BETTING.Settings.AllowAllInBets then table.insert(buttons, {text = "All-In!", allinbutton = true, color = Color(0,61,245), func = function(amt,betply)
		Derma_Query( string.format("Are you sure you want to bet all your %s?", BETTING.GetPointsName()), "All In Bet", "Yes", function()
		if not BettingMainWindow then
			local result,msg = hook.Call("PlayerCanBet",nil,LocalPlayer())
			BETTING.ShowChatNotice(nil, msg or "Bet cancelled.")
			PlaceBetPanel:Close() 
			return 
		end
		func(amt,betply,true) end, "Cancel", function() end) 
	end}) end
	table.insert(buttons, {text = "Cancel", color = Color(255,51,51), func = function() end, cancel = true})
	for k,v in pairs(buttons) do
		local Button = vgui.Create("PSBettingButton")
		Button:SetText(v.text)
		Button:SetColor(v.color)
		Button.DoClick = function() 
			if not v.cancel and BetWarning:IsVisible() and not v.allinbutton then BETTING.PlaySound("resource/warning.wav") return end 
			if BETTING.Settings.DisableCursorAfterBetPlaced then gui.EnableScreenClicker(false) end
			if not BettingMainWindow then
				if v.cancel then PlaceBetPanel:Close() return end
				local result,msg = hook.Call("PlayerCanBet",nil,LocalPlayer())
				BETTING.ShowChatNotice(nil, msg or "Bet cancelled.")
				PlaceBetPanel:Close() 
			return end
			v.func(NumberWang:GetValue(),PlaceBetPanel.Player) 
			PlaceBetPanel:Close() 
			end
		ButtonList:AddItem(Button)
		SmartScroller:AddPanel(Button)
	end
	
	if ( #buttons == 0 ) then
		PlaceBetPanel:Close()
		ErrorNoHalt( "Pointshop Betting Error: UI Input options invalid." )
		return
	end
	
	PlaceBetPanel.OnKeyCodePressed = function(self,key) 
		if IsValid(self) and (key == KEY_ENTER) then 
			if BetWarning:IsVisible() then BETTING.PlaySound("resource/warning.wav") return end 
			if BETTING.Settings.DisableCursorAfterBetPlaced then gui.EnableScreenClicker(false) end
			if not BettingMainWindow then
				local result,msg = hook.Call("PlayerCanBet",nil,LocalPlayer())
				BETTING.ShowChatNotice(nil, msg or "Bet cancelled.")
				PlaceBetPanel:Close() 
			return end
			func(NumberWang:GetValue(),PlaceBetPanel.Player) PlaceBetPanel:Close()
		end
	end
	
	NumberWang:RequestFocus()
	NumberWang:SelectAllText( true )

	PlaceBetPanel:MakePopup()
	//PlaceBetPanel:DoModal()
	
	if timeout then
		timer.Simple(30, function() if IsValid(PlaceBetPanel) then PlaceBetPanel:Close() end end)
	end
end

function BETTING.PlaySound(file,volume)
	if not file or not BETTING.Settings.EnableSounds or not system.HasFocus() then return end
	if string.StartWith(file,"http://") or string.StartWith(file,"www.") then
		sound.PlayURL(file,"", function(audio)
			if IsValid(audio) and volume then
				audio:Play()
				audio:SetVolume(math.Clamp(volume,0,1))
			end
		end)
	else surface.PlaySound(file)
	end
end