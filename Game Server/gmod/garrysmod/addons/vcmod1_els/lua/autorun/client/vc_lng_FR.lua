// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Francais"
Lng.Language_Code = "FR"
Lng.Translated_By_Name = "SaViix"
Lng.Translated_By_Link = "http://steamcommunity.com/id/SaViix"
Lng.Translated_Date = "2015 03 22"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Véhicule verrouillé."
Lng.UnLocked = "Véhicule déverrouillé."
Lng.Chat = 'Pour régler les options (feux de voiture, vue) tapez "!vcmod" dans le chat.'
Lng.Broken = "Ce véhicule a eu un accident, il a besoin d'être réparé."
Lng.Trl_Atch = "Remorque attachée."
Lng.Trl_Detch = "Remorque détaché."
Lng.ELS_TuningIntoPoliceC = "Vous êtes connecter a la radio de police"
Lng.ELS_NoPoliceRCFound = "Aucune radio de police n'a été trouvée"

////////Pickup
Lng.TouchCar100 = "Touchez un vehicule pour la réparer à 100%."
Lng.TouchCar25 = "Touchez un vehicule pour rajouter +25% de sa vie original."
Lng.TouchCar10 = "Touchez un vehicule pour rajouter +10% de sa vie original."

////////MENU general
Lng.Info = "Infos"
Lng.Menu = "Menu"
Lng.Language = "Language"
Lng.Personal = "Personnel"
Lng.Administrator = "Administrateur"
Lng.Options = "Options"
Lng.ELSOptions = "ELS Options"
Lng.Main = "Principal"
Lng.Controls = "Contrôles"
Lng.HUD = "HUD"
Lng.View = "Vue"
Lng.Radio = "Radio"
Lng.Multiplier = "Multiplicateur"
Lng.OptOnly_You = "Ces options sont affectées que sur vous"
Lng.OptOnly_Admin = "Les paramètres peuvent être modifiés que par un administrateur"
Lng.NPC_Settings = "Paramètres NPC"
Lng.Height = "Hauteur"
Lng.FadeOutDistance = "Distance d'affichage"
Lng.Reset = "Réinitialiser"
Lng.Save = "Sauvegarder"
Lng.Load = "Charger"
Lng.Volume = "Volume"
Lng.Distance = "Distance"
Lng.None = "Aucun"
Lng.EnterKey = "Entrer la touche"

Lng.Enabled_Cl = "Activer le côté client"
Lng.Enabled_Sv = "Activer le côté serveur"

Lng.Lights = "Lumières"
Lng.Health = "Santé"
Lng.Sound = "Son"
Lng.Other = "Autre"
Lng.CreatedBy = "créé par"
Lng.Enabled = "Activé"
Lng.OffTime = "Temps pour éteindre"
Lng.Time = "Temps"
Lng.DistMultiplier = "Distance multiplicateur"
Lng.ControlsReset = "Contrôles réinitialisés."
Lng.SettingsSaved = "Paramètres sauvegardés."
Lng.SettingsReset = "Paramètres réinitialisés."
Lng.LoadedSettingsFromServer = "Paramètres chargés sur le serveur."
Lng.InteriorIndicators = "Indicateurs intérieur"
Lng.ExtraGlow = "Éclat supplémentaire"

////////MENU ELS
Lng.ManulSiren = "Sirène manuel"
Lng.ELS_SirenSwitch = "ELS Interrupteur de sirène"
Lng.ELS_SirenToggle = "ELS basculer les sirene"
Lng.ELS_LightsSwitch = "ELS Interrupteur Gyrophare"
Lng.ELS_LightsToggle = "ELS Basculer les lumières"
Lng.VCModMainEnabled = "VCMod Principale activée"
Lng.VCModELSEnabled = "VCMod ELS activé"
Lng.ELSLightsEnabled = "ELS lumières activées"
Lng.AutoDisableELSLights = "Désactivation auto des lumières ELS"
Lng.OffOnExit = "Éteindre en sortant"
Lng.Siren = "Sirène"
Lng.AutoDisableELSSounds = "Désactivation auto des des sons ELS"
Lng.Manual = "Manuel"
Lng.Bullhorn = "Sirène police"
Lng.PoliceChatterEnabled = "Bavardage de la police activée"
Lng.PoliceChatter = "Bavardage de la police"
Lng.PoliceChatter_Info = "Le bavardage de la police est un véritable simulateur de radio de police"
Lng.SelectedRadioChatter = "Sélectionné le canal de la police"
Lng.ReduceDamageToEmergencyVehicles = "Réduire les dommages aux véhicules d'urgence"

////////MENU personal info
Lng.YouAreUsingVCMod = "Vous utilisez VCMod"
Lng.ServerIsUsingVCMod = "Ce serveur utilise VCMod"
Lng.Info_EverThought = "VCMod est conçu pour rendre les véhicules de Garry's Mod aussi bon que dans n'importe quel autre jeu."
Lng.Info_VCModHasFollowingAddons = "VCMod a ces addons suivants:"

////////MENU personal options
Lng.VisDist = "La distance de visibilité"
Lng.Warmth = "Chaleur"
Lng.Lines = "Lignes"
Lng.Glow = "Lueur"
Lng.DynamicLights = "Lumières dynamiques"
Lng.ThirdPView = "Troisième personne ( vue )"
Lng.DynamicView = "Vue dynamique"
Lng.AutoFocus = "Auto-caméra"
Lng.Reverse = "Inverse"
Lng.VectorStiffness = "Direction Résister %"
Lng.AngleStiffness = "Angle Résister %"
Lng.IgnoreWorld = "ignorer le monde"
Lng.TruckView = "Adapter la vue du camion quand une remorque et attacher"

////////MENU controls
Lng.HoldDuration = "Durée a maintenir"
Lng.Mouse = "Souris"
Lng.KeyboardInput = "Contrôle avec le clavier"
Lng.MouseInput = "Contrôle avec la souris"

Lng.NightLights = "Lumières de nuit"
Lng.HeadLights = "Phares avant"
Lng.LowHigh = "Faible / Élevé pour basculer les plein phare"
Lng.HazardLights = "Feux de détresse"
Lng.BlinkerLeft = "Clignotant gauche"
Lng.BlinkerRight = "Clignotant droite"
Lng.Horn = "Klaxon"
Lng.Cruise = "Limitateur de vitesse"
Lng.LockUnlock = "Verrouillage / déverrouillage"
Lng.LookBehind = "Regarder en arrière"
Lng.DetachTrl = "Décrocher la remorque"

////////MENU hud
Lng.Effect3D = "Effet 3D"
Lng.HUDHeight = "Hauteur du HUD %"
Lng.HUD_Name = "Nom"
Lng.HUD_Icons = "Icônes"
Lng.HUD_Cruise = "Limitateur de vitesse"
Lng.HUD_Cruise_MPH = "mi/h au lieu de km/h"
Lng.HUD_Repair = "Réparation"
Lng.HUD_ELS_Siren = "ELS Sirène"
Lng.HUD_ELS_Lights = "ELS Lumière"

////////MENU admin options
Lng.HandbrakeLights = "Feux de frein à main"
Lng.InteriorLights = "Éclairage intérieur"
Lng.BlinkersOffExit = "Éteindre les clignotant en sortant du véhicule"

Lng.DamageEnabled = "Activée les dommages"
Lng.StartHealthMultiplier = "Multiplicateur de santé"
Lng.PhysicalDamage = "Dommages physiques"
Lng.FireDuration = "Durée d'incendie"
Lng.RemoveCarAfterExplosion = "Retirer la voiture après l'explosion"
Lng.ReducePlayerDmgWhileInCar = "Réduire les dommages du joueur quand il et dans une voiture"

Lng.DoorSounds = "Sons des porte"
Lng.TruckRevBeep = "Alarme marche arrière pour les camion"

Lng.SteeringWheelLOnExit = "Verrouillage du volant à la sortie"
Lng.BrakesLOnExit = "Blocage des freins à la sortie"
Lng.WheelDust = "Poussière des roues"
Lng.WheelDustWhileBraking = "La poussière des roue lors du freinage"
Lng.MatchPlayerSpeedExit = "Quand le joueur sort du véhicule il garde la même vitesse"
Lng.NoCollidePlyOnExit = "Pas de collision lorsqu'un joueur sort du véhicule"
Lng.Exhaust = "Echappement"
Lng.PassengerSeats = "Les sièges des passagers"
Lng.TrailerAttach = "Joindre une remorque"
Lng.TrailerAttachConStrengthM = "Puissance pour joindre une remorque( multiplicateur )"
Lng.TrailersCanAtchToReg = "La Remorque peut être attacher à une voiture"
Lng.RepairToolSpeedMult = "Vitesse de la réparation avec l'outils ( Multiplicateur )"



if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng