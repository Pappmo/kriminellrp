﻿// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Español"
Lng.Language_Code = "ES"
Lng.Translated_By_Name = "Alejandriiko"
Lng.Translated_By_Link = "http://steamcommunity.com/id/iAlexLng/"
Lng.Translated_Date = "2015 03 27"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Vehiculo Bloqueado."
Lng.UnLocked = "Vehiculo Desbloqueado."
Lng.Chat = 'Para ajustar las opciones (luces de los coches, vista) escribe "!vcmod" en el chat.'
Lng.Broken = "El vehiculo esta roto, necesita ser reparado."
Lng.Trl_Atch = "Remolque enganchado."
Lng.Trl_Detch = "Remolque desenganchado."
Lng.ELS_TuningIntoPoliceC = "Sintinizando canal policial."
Lng.ELS_NoPoliceRCFound = "No se encontró ningún canal policial."

////////Pickup
Lng.TouchCar100 = "Toca el coche para repararlo al 100%."
Lng.TouchCar25 = "Toca el coche para repararlo un 25%."
Lng.TouchCar10 = "Toca el coche para repararlo un 10%."

////////MENU general
Lng.Info = "Info"
Lng.Menu = "Menú"
Lng.Language = "Idioma"
Lng.Personal = "Personal"
Lng.Administrator = "Administrador"
Lng.Options = "Opciones"
Lng.ELSOptions = "ELS Opciones"
Lng.Main = "Principal"
Lng.Controls = "Controles"
Lng.HUD = "HUD"
Lng.View = "Vista"
Lng.Radio = "Radio"
Lng.Multiplier = "Multiplier"
Lng.OptOnly_You = "Estas opciones solo te afectan a ti."
Lng.OptOnly_Admin = "Estas opciones solo pueden ser modificadas por un administrador."
Lng.NPC_Settings = "Configuración del NPC"
Lng.Height = "Altura"
Lng.FadeOutDistance = "Fuera de rango"
Lng.Reset = "Resetear"
Lng.Save = "Guardar"
Lng.Load = "Cargar"
Lng.Volume = "Volumen"
Lng.Distance = "Distancia"
Lng.None = "Ninguna"
Lng.EnterKey = "Selecciona una tecla"

Lng.Enabled_Cl = "Clientside activado"
Lng.Enabled_Sv = "Serverside activado"

Lng.Lights = "Luces"
Lng.Health = "Vida"
Lng.Sound = "Sonido"
Lng.Other = "Otros"
Lng.CreatedBy = "Creado por"
Lng.Enabled = "Activado"
Lng.OffTime = "Fuera de tiempo"
Lng.Time = "Tiempo"
Lng.DistMultiplier = "Multiplicador de distancia"
Lng.ControlsReset = "Controles reseteados."
Lng.SettingsSaved = "Configuración Guardada"
Lng.SettingsReset = "Configuración reseteada."
Lng.LoadedSettingsFromServer = "Configuración cargada al servidor."
Lng.InteriorIndicators = "Indicadores interiores"
Lng.ExtraGlow = "Resplandor adicional"

////////MENU ELS
Lng.ManulSiren = "Sirena Manual"
Lng.ELS_SirenSwitch = "ELS Interruptor de sirena"
Lng.ELS_SirenToggle = "ELS Palanca de sirena"
Lng.ELS_LightsSwitch = "ELS Interruptor de luces"
Lng.ELS_LightsToggle = "ELS Palanca de luces"
Lng.VCModMainEnabled = "VCMod principal activado"
Lng.VCModELSEnabled = "VCMod ELS activadas"
Lng.ELSLightsEnabled = "ELS luces activadas"
Lng.AutoDisableELSLights = "Auto desactivar luces de ELS"
Lng.OffOnExit = "Apagar al salir"
Lng.Siren = "Sirena"
Lng.AutoDisableELSSounds = "Auto desactivar sonidos de ELS"
Lng.Manual = "Manual"
Lng.Bullhorn = "Megáfono"
Lng.PoliceChatterEnabled = "Canal policial activado"
Lng.PoliceChatter = "Canal policial"
Lng.PoliceChatter_Info = "La radio policial es un canal de los policias para hablar mediante la radio."
Lng.SelectedRadioChatter = "Seleccionado un canal de conversaciones por radio"
Lng.ReduceDamageToEmergencyVehicles = "Reducir el daño de los vehiculos de emergencia"

////////MENU personal info
Lng.YouAreUsingVCMod = "Usted está usando VCMod"
Lng.ServerIsUsingVCMod = "El servidor esta usando VCMod"
Lng.Info_EverThought = "¿Alguna vez has pensado que los vehículos de GMOD no son cosas reales? VCMod está diseñado para hacer los vehículos garrysmod tan reales como en cualquier otro juego."
Lng.Info_VCModHasFollowingAddons = "VCMod tiene los siguientes complementos:"

////////MENU personal options
Lng.VisDist = "Distancia de visión"
Lng.Warmth = "Calidez"
Lng.Lines = "Líneas"
Lng.Glow = "Brillo"
Lng.DynamicLights = "Luces dinámicas"
Lng.ThirdPView = "Modo tercera persona"
Lng.DynamicView = "Visión dinámica"
Lng.AutoFocus = "Enfocar automáticamente"
Lng.Reverse = "Deshacer"
Lng.VectorStiffness = "Vector de rigidez %"
Lng.AngleStiffness = "Ángulo de rigidez %"
Lng.IgnoreWorld = "Ignorar mundo"
Lng.TruckView = "Vista del camión conectada"

////////MENU controls
Lng.HoldDuration = "Duración al mantener"
Lng.Mouse = "Ratón"
Lng.KeyboardInput = "Entrada del teclado"
Lng.MouseInput = "Entrada del ratón"

Lng.NightLights = "Luces nocturnas"
Lng.HeadLights = "Luces delanteras"
Lng.LowHigh = "Bajo/Alto destello"
Lng.HazardLights = "Luces de emergencia"
Lng.BlinkerLeft = "Intermitente izquierdo"
Lng.BlinkerRight = "Intermitente derecho"
Lng.Horn = "Claxon"
Lng.Cruise = "Modo Crucero"
Lng.LockUnlock = "Bloquear/Desbloquear"
Lng.LookBehind = "Mirar atrás"
Lng.DetachTrl = "Separar remolque"

////////MENU hud
Lng.Effect3D = "Efecto 3D"
Lng.HUDHeight = "HUD Altura %"
Lng.HUD_Name = "Nombre"
Lng.HUD_Icons = "Iconos"
Lng.HUD_Cruise = "Modo Crucero"
Lng.HUD_Cruise_MPH = "mi/h en lugar de km/h"
Lng.HUD_Repair = "Reparar"
Lng.HUD_ELS_Siren = "Sirenas de ELS"
Lng.HUD_ELS_Lights = "Luces de ELS"

////////MENU admin options
Lng.HandbrakeLights = "Luces de freno de mano"
Lng.InteriorLights = "Luces del interior"
Lng.BlinkersOffExit = "Intermitentes (luces de giro) apagar al salir"

Lng.DamageEnabled = "Daño activado"
Lng.StartHealthMultiplier = "Iniciar multiplicador de salud"
Lng.PhysicalDamage = "Daño físico"
Lng.FireDuration = "Duración del fuego"
Lng.RemoveCarAfterExplosion = "Eliminar el coche después de la explosión"
Lng.ReducePlayerDmgWhileInCar = "Reducir el daño del conductor mientras conduce"

Lng.DoorSounds = "Sonido de las puertas"
Lng.TruckRevBeep = "Sonido del camión marcha atrás"

Lng.SteeringWheelLOnExit = "Bloquear volante al salir"
Lng.BrakesLOnExit = "Bloquear frenos al salir"
Lng.WheelDust = "Llantas"
Lng.WheelDustWhileBraking = "Llantas mientras derrapan"
Lng.MatchPlayerSpeedExit = "Velocidad de salida del jugador"
Lng.NoCollidePlyOnExit = "Los jugadores no chocan con el coche al salir de él"
Lng.Exhaust = "Tubos de escapes"
Lng.PassengerSeats = "Asientos de pasajeros"
Lng.TrailerAttach = "Enganchar el remolque"
Lng.TrailerAttachConStrengthM = "Fuerza de la conexión al enganchar un remolque"
Lng.TrailersCanAtchToReg = "Un remolque se puede enganchar en cualquier coche"
Lng.RepairToolSpeedMult = "Herramienta para reparar el multiplicador de la velocidad"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng