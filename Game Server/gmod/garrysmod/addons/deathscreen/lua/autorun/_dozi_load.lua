-----------------------------------------------------------------
-- @package		Dozi
-- @author		Richard
-- @build 		v1.3
-- @release		09.19.2015
-----------------------------------------------------------------

Dozi = Dozi or {}
Software = Dozi.Software or {}
Software.Name = "Dozi"
Software.Author = "Richard"
Software.Build = "1.3"
Software.Released = "Sep 19, 2015"

local luaroot = "dozi"
local name = "Dozi"

local doziIntro = {}

for k, i in ipairs( doziIntro ) do 
	MsgC( Color( 255, 179, 3 ), i .. '\n' )
end

if SERVER then
	AddCSLuaFile( )
	local folder = luaroot .. "/sh"
	local files = file.Find( folder .. "/" .. "*.lua", "LUA" )
	for _, file in ipairs( files ) do
		AddCSLuaFile( folder .. "/" .. file )
	end

	folder = luaroot .."/cl"
	files = file.Find( folder .. "/" .. "*.lua", "LUA" )
	for _, file in ipairs( files ) do
		AddCSLuaFile( folder .. "/" .. file )
	end

	--Shared modules
	local files = file.Find( luaroot .."/sh/*.lua", "LUA" )
	if #files > 0 then
		for _, file in ipairs( files ) do
			MsgC(Color( 255, 179, 3), "[" .. Software.Name .. "] Loading SHARED file: " .. file .. "\n")
			include( luaroot .. "/sh/" .. file )
			AddCSLuaFile( luaroot .. "/sh/" .. file )
		end
	end

	--Server modules
	local files = file.Find( luaroot .."/sv/*.lua", "LUA" )
	if #files > 0 then
		for _, file in ipairs( files ) do
			MsgC(Color( 255, 179, 3 ), "[" .. Software.Name .. "] Loading SERVER file: " .. file .. "\n")
			include( luaroot .. "/sv/" .. file )
		end
	end

	MsgC(Color( 255, 179, 3 ), "\n----------------------------[ Load Complete ]------------------------\n\n")
end

if CLIENT then
	--Shared modules
	local files = file.Find( luaroot .."/sh/*.lua", "LUA" )
	if #files > 0 then
		for _, file in ipairs( files ) do
			MsgC(Color( 255, 179, 3 ), "[" .. Software.Name .. "] Loading SHARED file: " .. file .. "\n")
			include( luaroot .. "/sh/" .. file )
		end
	end

	--Client modules
	local files = file.Find( luaroot .."/cl/*.lua", "LUA" )
	if #files > 0 then
		for _, file in ipairs( files ) do
			MsgC(Color( 255, 179, 3 ), "[" .. Software.Name .. "] Loading CLIENT file: " .. file .. "\n")
			include( luaroot .."/cl/" .. file )
		end
	end
	MsgC(Color( 255, 179, 3 ), "-------------------------------[ Load Complete ]---------------------------\n\n")
end