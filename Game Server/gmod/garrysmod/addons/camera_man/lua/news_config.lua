NewsConfig = NewsConfig or {}
NewsConfig.jobIsVIP = false
NewsConfig.vipGroups = {"donator", "donor", "vip", "special"}
NewsConfig.jobPay = 69
NewsConfig.jobIsVoted = false
NewsConfig.jobMax = 3
NewsConfig.entityPrice = 0
NewsConfig.entityMax = 2
NewsConfig.entityIsVIP = false
NEWS_SOUNDS = {
	{"Male Scream", "ambient/voices/m_scream1.wav", "icon16/user.png"},
	{"Female Scream", "ambient/voices/f_scream1.wav", "icon16/user_female.png"},
	{"Cough", {"ambient/voices/cough1.wav", "ambient/voices/cough2.wav", "ambient/voices/cough3.wav", "ambient/voices/cough4.wav"}, "icon16/bug.png"},
	{"Riot", "ambient/levels/streetwar/city_riot2.wav", "icon16/group.png"},
	{"Explosion", {
			"ambient/explosions/explode_1.wav",
			"ambient/explosions/explode_2.wav",
			"ambient/explosions/explode_3.wav",
			"ambient/explosions/explode_4.wav",
			"ambient/explosions/explode_5.wav",
			"ambient/explosions/explode_9.wav"
		},
		"icon16/bomb.png"
	},
	{"Train Horn", "ambient/alarms/train_horn2.wav", "icon16/lorry.png"},
	{"Alarm", "ambient/alarms/klaxon1.wav", "icon16/exclamation.png"},
	{"Thunder", {
			"ambient/weather/thunder1.wav",
			"ambient/weather/thunder2.wav",
			"ambient/weather/thunder3.wav",
			"ambient/weather/thunder4.wav",
			"ambient/weather/thunder5.wav",
			"ambient/weather/thunder6.wav"
		},
		"icon16/weather_rain.png"
	}
}