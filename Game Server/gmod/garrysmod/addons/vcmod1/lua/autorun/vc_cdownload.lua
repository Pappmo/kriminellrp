// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

-- Sounds:
-- resource.AddFile("sound/vcmod/blink_in.wav")
-- resource.AddFile("sound/vcmod/blink_out.wav")
-- resource.AddFile("sound/vcmod/clk.wav")
-- resource.AddFile("sound/vcmod/door_close.wav")
-- resource.AddFile("sound/vcmod/door_open.wav")
-- resource.AddFile("sound/vcmod/handbrake.wav")
-- resource.AddFile("sound/vcmod/switch.wav")
-- resource.AddFile("sound/vcmod/switch_off.wav")
-- resource.AddFile("sound/vcmod/switch_on.wav")

-- resource.AddFile("sound/vcmod/clk.wav")
-- resource.AddFile("sound/vcmod/clk_els.wav")
-- resource.AddFile("sound/vcmod/horn/heavy.wav")
-- resource.AddFile("sound/vcmod/horn/light.wav")

-- Materials:
-- resource.AddFile("materials/vcmod/light_hd.vmt")
-- resource.AddFile("materials/vcmod/light_hd.vtf")
-- resource.AddFile("materials/vcmod/headlight_beam_dip.vmt")
-- resource.AddFile("materials/vcmod/headlight_beam_dip.vtf")

-- Episode 2 content:
-- resource.AddFile("materials/particle/debris1/debris.vmt")
-- resource.AddFile("materials/particle/debris1/debris.vtf")

-- Fast DL:
if SERVER then resource.AddWorkshop("341079174") end