-----------------------------------------------------------------
-- @package     HUDHive
-- @author      Richard
-- @build       v1.2.1
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ TABLES ]
-----------------------------------------------------------------
HUDHive = HUDHive or {}
HUDHive.Settings = HUDHive.Settings or {}
HUDHive.Language = {}

HUDHive.Script = HUDHive.Script or {}
HUDHive.Script.Name = "HUDHive"
HUDHive.Script.Author = "Richard"
HUDHive.Script.Build = "1.2.1"
HUDHive.Script.SFid = "1770"
HUDHive.Script.Released = "Dec 29, 2015"
HUDHive.Script.Website = "https://scriptfodder.com/scripts/view"

-----------------------------------------------------------------
-- [ AUTOLOADER ]
-----------------------------------------------------------------

local luaroot = "hudhive"
local name = "HUDHive"
local langLib =  luaroot .. "/sh/lang"

local HUDHiveStartupHeader = {
    '\n\n',
    [[.................................................................... ]],
}

local HUDHiveStartupInfo = {
    [[[title]....... ]] .. HUDHive.Script.Name .. [[ ]],
    [[[build]....... v]] .. HUDHive.Script.Build .. [[ ]],
    [[[released].... ]] .. HUDHive.Script.Released .. [[ ]],
    [[[author]...... ]] .. HUDHive.Script.Author .. [[ ]],
    [[[website]..... ]] .. HUDHive.Script.Website .. "/" .. HUDHive.Script.SFid .. [[ ]],
}

local HUDHiveStartupFooter = {
    [[.................................................................... ]],
}

function HUDHive:PerformCheck(func)
    if (type(func)=="function") then
        return true
    end
    
    return false
end

function game.GetIP()
    local hostip = GetConVarString( "hostip" )
    hostip = tonumber( hostip )
    local ip = {}
    ip[ 1 ] = bit.rshift( bit.band( hostip, 0xFF000000 ), 24 )
    ip[ 2 ] = bit.rshift( bit.band( hostip, 0x00FF0000 ), 16 )
    ip[ 3 ] = bit.rshift( bit.band( hostip, 0x0000FF00 ), 8 )
    ip[ 4 ] = bit.band( hostip, 0x000000FF )
    return table.concat( ip, "." )
end

-----------------------------------------------------------------
-- Trying to leak it? Why don't you go get a real job
-- instead of me paying my taxes on supporting you eating
-- and playing with yourself on the internet.
-----------------------------------------------------------------



for k, i in ipairs( HUDHiveStartupHeader ) do 
    MsgC( Color( 255, 255, 0 ), i .. '\n' )
end

for k, i in ipairs( HUDHiveStartupInfo ) do 
    MsgC( Color( 255, 255, 255 ), i .. '\n' )
end

for k, i in ipairs( HUDHiveStartupFooter ) do 
    MsgC( Color( 255, 255, 0 ), i .. '\n' )
end

-----------------------------------------------------------------
-- [ SERVER-SIDE ACTIONS ]
-----------------------------------------------------------------

if SERVER then

    local fol = luaroot .. "/"
    local files, folders = file.Find(fol .. "*", "LUA")

    for k, v in pairs(files) do
        include(fol .. v)
    end

	MsgC(Color( 255, 255, 0 ), "\n[ Loading Files ]\n\n")

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/sh_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[shared]........." .. File .. "\n")
            AddCSLuaFile(fol .. folder .. "/" .. File)
            include(fol .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/sv_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[server]........." .. File .. "\n")
            include(fol .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/cl_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[client]........." .. File .. "\n")
            AddCSLuaFile(fol .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(fol .. folder .. "/vgui_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[client]........." .. File .. "\n")
            AddCSLuaFile(fol .. folder .. "/" .. File)
        end
    end

end

-----------------------------------------------------------------
-- [ CLIENT-SIDE ACTIONS ]
-----------------------------------------------------------------

if CLIENT then

    local root = "hudhive" .. "/"
    local _, folders = file.Find(root .. "*", "LUA")

	MsgC(Color( 255, 255, 0 ), "\n[ Files ]\n")

    for _, folder in SortedPairs(folders, true) do
        if folder == "." or folder == ".." then continue end

        for _, File in SortedPairs(file.Find(root .. folder .. "/sh_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[shared]........." .. File .. "\n")
            include(root .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        for _, File in SortedPairs(file.Find(root .. folder .. "/cl_*.lua", "LUA"), true) do
            MsgC(Color(255, 255, 0), "[client]........." .. File .. "\n")
            include(root .. folder .. "/" .. File)
        end
    end

    for _, folder in SortedPairs(folders, true) do
        for _, File in SortedPairs(file.Find(root .. folder .. "/vgui_*.lua", "LUA"), true) do
            MsgC(Color(255, 0, 0), "[vgui]..........." .. File .. "\n")
            include(root .. folder .. "/" .. File)
        end
    end

end

MsgC(Color( 255, 255, 0 ), "\n..........................[ HUDHive Loaded ]........................\n\n")

local function MOTDDraw(text)
    MsgC(Color( 0, 255, 0 ), "\n..........................[ HUDHive MOTD ]..........................\n\n")
    MsgC(Color(255, 255, 255, 255), text)
    MsgC(Color( 0, 255, 0 ), "\n....................................................................\n\n")
end

local function MOTDFetch(body, len, headers, code)
    if len > 0 and headers and code == 200 then
		MOTDDraw(body)
	else
		MOTDDraw("An error occured fetching the MOTD. Please try again later.")
    end
end

local function MOTDShow()
	local MOTDRequest = "http://api.galileomanager.com/products/" .. HUDHive.Script.SFid .. "/motd.txt"
    http.Fetch(MOTDRequest, MOTDFetch)
end
timer.Simple(8, MOTDShow)

local function UpdaterDraw(release_date, release_version)
    MsgC(Color( 0, 255, 0 ), "\n.........................[ HUDHive Updater ]........................\n\n")
	if release_version > HUDHive.Script.Build then
		MsgC(Color(255, 255, 255, 255), "An Update Is Available!\n\n")
		MsgC(Color(255, 255, 255, 255), "[version installed]........v" .. HUDHive.Script.Build .. "\n")
		MsgC(Color(255, 255, 255, 255), "[update available].........v" .. release_version .. "\n")
		MsgC(Color(255, 255, 255, 255), "[update released].........." .. release_date .. "\n\n")
		MsgC(Color(255, 255, 255, 255), "We strongly suggest updating the script to the latest version.\n")
		MsgC(Color(255, 255, 255, 255), "This ensures that bugs are patched and you are getting the best\n")
		MsgC(Color(255, 255, 255, 255), "experience. \n\n")

		MsgC(Color(255, 255, 255, 255), "You can download the latest copy of this script by visiting:\n")
		MsgC(Color(255, 255, 255, 255), HUDHive.Script.Website .. "/" .. HUDHive.Script.SFid .. "\n")
	else
		MsgC(Color(255, 255, 255, 255), "You are up-to-date!\n\n")
		MsgC(Color(255, 255, 255, 255), "[version installed]........v" .. HUDHive.Script.Build .. "\n")
		MsgC(Color(255, 255, 255, 255), "[version released]........." .. HUDHive.Script.Released .. "\n")
	end
    MsgC(Color( 0, 255, 0 ), "\n....................................................................\n\n")
end

local function UpdaterFetch(body, len, headers, code)

	local entry 			= util.JSONToTable( body )
	local releaseTable 		= entry["" .. HUDHive.Script.Name .. ""]["version"][1]
	local release_date 		= releaseTable["release"]
	local release_version 	= releaseTable["version"]

    if len > 0 and headers and code == 200 and releaseTable then
		UpdaterDraw(release_date, release_version)
	else
		UpdaterDraw("The updater has encountered an error. Check again later.")
    end
end

local function UpdaterShow()
	local UpdaterRequest = "http://api.galileomanager.com/products/" .. HUDHive.Script.SFid .. "/index.php?type=json"
    http.Fetch(UpdaterRequest, UpdaterFetch)
end
timer.Simple(2, UpdaterShow)

