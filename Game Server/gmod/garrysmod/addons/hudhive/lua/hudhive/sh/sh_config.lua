-----------------------------------------------------------------
-- @package     HUDHive
-- @author      Richard
-- @build       v1.2.1
-- @release     12.29.2015
-----------------------------------------------------------------

HUDHive = HUDHive or {}
HUDHive.Settings = HUDHive.Settings or {}
HUDHive.Language = HUDHive.Language or {}

-----------------------------------------------------------------
-- [ WORKSHOP / FASTDL RESOURCES ]
-----------------------------------------------------------------
-- Set [HUDHive.Settings.ResourcesEnabled] to false if you do not 
-- wish for the server to force players to download the 
-- resources/materials.
-----------------------------------------------------------------
-- Set [HUDHive.Settings.WorkshopEnabled] to false if you do not 
-- wish for the server to force clients to download the resources 
-- from the workshop.
-----------------------------------------------------------------

HUDHive.Settings.ResourcesEnabled = true
HUDHive.Settings.WorkshopEnabled = true

-----------------------------------------------------------------
-- [ LANGUAGE ]
-----------------------------------------------------------------

HUDHive.Language = {
    arrested = "Arrested",
    announcement = "Announcement",
    money = "Money",
    salary = "Salary",
    ammoclip = "Clip",
    ammoexta = "Extra",
    level = "Level",
    lockdown_active = "Lockdown Active",
    wanted = "Wanted",
}

-----------------------------------------------------------------
-- [ MAIN GUI SETTINGS ]
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainBoxWidth ]
-- 
-- The WIDTH of the main HUD box (usually bottom-left)
-- Keep in mind that I use IMAGES for my HUD. So if you're going
-- to use the same images I did, it may look off. This setting
-- is for people using transparent background colors, or to make 
-- their own images for the HUD.
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainBoxHeight ]
-- 
-- The HEIGHT of the main HUD box (usually bottom-left)
-- Keep in mind that I use IMAGES for my HUD. So if you're going
-- to use the same images I did, it may look off. This setting
-- is for people using transparent background colors, or to make 
-- their own images for the HUD.
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainPosition ]
-- 
-- Positioning:
--      1 = topLeft
--      2 = topRight
--      3 = bottomLeft
--      4 = bottomRight
-----------------------------------------------------------------

HUDHive.Settings.MainBoxWidth = 300
HUDHive.Settings.MainBoxHeight = 155
HUDHive.Settings.MainPosition = 3

-----------------------------------------------------------------
-- [ HUDHive.Settings.MainAnimatedAvatarEnabled ]
-- 
-- true     :   The animated playermodel will display
-- false    :   Steam avatar picture will be displayed
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainAnimatedFOV ]
-- 
-- The Field of View for the animated player model.
-- Default: 12
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainAnimatedCamPOS ]
-- 
-- Camera position for animated player model.
-- Default: Vector( 85, -22, 65 ) 
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainAnimatedLookAt ]
-- 
-- The camera position for looking at the player model from front.
-- Default: Vector( -2, 0, 62 )
-----------------------------------------------------------------
-- [ HUDHive.Settings.MainAvatarTeamBGColor ]
-- 
-- The 5 pixel border around the avatar image/model. 
-- true 	: 	The border will become the color of the player's
-- 				team color.
-- false 	:	Will use standard color assigned to the value 
-- 				HUDHive.Settings.MainAvatarBGColor
-----------------------------------------------------------------

HUDHive.Settings.MainAnimatedAvatarEnabled = true
HUDHive.Settings.MainAnimatedFOV = 12
HUDHive.Settings.MainAnimatedCamPOS = Vector( 85, -22, 65 )
HUDHive.Settings.MainAnimatedLookAt = Vector( -2, 0, 62 )
HUDHive.Settings.MainAvatarTeamBGColor = false
HUDHive.Settings.MainAvatarBGColor = Color( 15, 15, 15, 200 )

-----------------------------------------------------------------
-- [ MAIN GUI SETTINGS CONTINUED ]
-----------------------------------------------------------------

HUDHive.Settings.MainBarBackgroundColor = Color( 15, 15, 15, 255 )
HUDHive.Settings.MainBarOutlineColor = Color( 80, 80, 80, 100 )
HUDHive.Settings.MainMoneyTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.MainMoneyTitleTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.MainMoneyIcon = "hudhive/hh_money.png"
HUDHive.Settings.MainMoneyIconColor = Color(8, 156, 40, 255)
HUDHive.Settings.MainSalaryTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.MainSalaryTitleTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.MainSalaryIcon = "hudhive/hh_salary.png"
HUDHive.Settings.MainSalaryIconColor = Color(163, 135, 79, 255)
HUDHive.Settings.MainPlayerNameTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.MainPlayerJobTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.MainUseIconsMoneySalary = true
HUDHive.Settings.MainStatusShowIcons = true
HUDHive.Settings.MainStatusPulseSpeed = 2
HUDHive.Settings.MainBarsUseIcons = true
HUDHive.Settings.MainBarsShowProgress = true

-----------------------------------------------------------------
-- [ BACKGROUNDS -> OPTION 1 -> WEB BASED ]
-----------------------------------------------------------------

HUDHive.Settings.BackgroundsEnable = true
HUDHive.Settings.BackgroundColorSolid = Color( 5, 5, 5, 230 )
HUDHive.Settings.Backgrounds = {
    "http://galileomanager.com/api/products/1700/resources/backgrounds/hudhive_default_main.png"
}

HUDHive.Settings.BackgroundsAgenda = {
    "http://galileomanager.com/api/products/1700/resources/backgrounds/hudhive_default_agenda.png"
}

HUDHive.Settings.BackgroundsOther = {
    "http://galileomanager.com/api/products/1700/resources/backgrounds/hudhive_default_other.png"
}

-----------------------------------------------------------------
-- [ BACKGROUNDS -> OPTION 2 -> WORKSHOP ]
-----------------------------------------------------------------
-- This section is used as a "backup" to the hud images. By
-- default, you can use images hosted from websites to use as 
-- images, but there's a backup in case you ever want to rely on 
-- workshop items.
-----------------------------------------------------------------

HUDHive.Settings.ImageGUIMain = "hudhive/default/hudhive_default_main.png"
HUDHive.Settings.ImageGUIAgenda = "hudhive/default/hudhive_default_agenda.png"
HUDHive.Settings.ImageGUIOther = "hudhive/default/hudhive_default_other.png"

-----------------------------------------------------------------
-- [ DATA FEATURE BLOCKS ]
-----------------------------------------------------------------

HUDHive.Settings.Features = {
    {
        enabled = true,
        blockType = "health",
        blockName = "HEALTH",
        blockMax = 100,
        blockLabelColor = Color( 255, 255, 255, 210 ),
        blockBarColor = Color(8, 156, 75, 190),
        blockIcon = "hudhive/hh_minify_hp.png",
        blockIconColor = Color( 255, 255, 255, 255 )
    },
    {
        enabled = true,
        blockType = "armor",
        blockName = "ARMOR",
        blockMax = 100,
        blockLabelColor = Color( 255, 255, 255, 210 ),
        blockBarColor = Color(163, 135, 79, 190),
        blockIcon = "hudhive/hh_minify_armor.png",
        blockIconColor = Color( 255, 255, 255, 255 )
    },
    {
        enabled = false,
        blockType = "stamina",
        blockName = "STAMINA",
        blockMax = 100,
        blockLabelColor = Color( 255, 255, 255, 210 ),
        blockBarColor = Color(64, 105, 126, 190),
        blockIcon = "hudhive/hh_minify_stamina.png",
        blockIconColor = Color( 255, 255, 255, 255 )
    },
    {
        enabled = true,
        blockType = "hunger",
        blockName = "HUNGER",
        blockMax = 100,
        blockLabelColor = Color( 255, 255, 255, 210 ),
        blockBarColor = Color(124, 51, 50, 190),
        blockIcon = "hudhive/hh_minify_hunger.png",
        blockIconColor = Color( 255, 255, 255, 255 )
    },
    {
        enabled = false,
        blockType = "xp",
        blockName = "XP",
        blockMax = 100,
        blockLabelColor = Color( 255, 255, 255, 210 ),
        blockBarColor = Color(145, 71, 101, 190),
        blockIcon = "hudhive/hh_minify_xp.png",
        blockIconColor = Color( 255, 255, 255, 255 )
    }
}

-----------------------------------------------------------------
-- [ XP / AMMO BLOCK ]
-----------------------------------------------------------------
-- [ HUDHive.Settings.XPAmmoPosition ]
-- Positioning:
--      1 = topLeft
--      2 = topRight
--      3 = bottomLeft
--      4 = bottomRight
-----------------------------------------------------------------

HUDHive.Settings.XPEnabled = true
HUDHive.Settings.XPAmmoPosition = 4
HUDHive.Settings.XPBoxLeftIcon = "hudhive/hh_leveling.png"
HUDHive.Settings.XPOriginalHudDisabled = true
HUDHive.Settings.XPIconColor = Color(255, 255, 255, 255)
HUDHive.Settings.XPBarEnabled = true
HUDHive.Settings.XPBoxLeftShowIcon = true
HUDHive.Settings.XPBoxLeftShowLevel = true
HUDHive.Settings.XPBoxLeftLevelTextColor = Color(255,255,255,255)
HUDHive.Settings.XPBoxRightShowProgress = false
HUDHive.Settings.XPBoxRightProgressTextColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.XPBarProgressColor = Color(8, 156, 75, 190)
HUDHive.Settings.XPBarBackgroundColor = Color(15,15,15,255)
HUDHive.Settings.XPBarOutlineColor = Color(80, 80, 80, 100)

HUDHive.Settings.AmmoBarEnabled = true
HUDHive.Settings.AmmoBoxWidth = 220
HUDHive.Settings.AmmoBoxHeight = 80
HUDHive.Settings.AmmoBarColor = Color(64, 105, 126, 190)
HUDHive.Settings.AmmoIcon = "hudhive/hh_bullet.png"
HUDHive.Settings.AmmoIconNormalColor = Color( 255, 255, 255 )
HUDHive.Settings.AmmoIconWarningColor = Color( 255, 30, 30 )
HUDHive.Settings.AmmoTextCountColor = Color( 255, 255, 255, 255 )
HUDHive.Settings.AmmoTextTitleColor = Color( 255, 255, 255, 255 )

-----------------------------------------------------------------
-- [ AGENDA BLOCK ]
-----------------------------------------------------------------
-- [ HUDHive.Settings.AgendaHideIfEmpty ]
--
-- true     :   Completey hides the agenda box if there isn't an 
--              agenda available at the moment.
-- 
-- false    :   Agenda box will display if job has agenda enabled
--              even if it's empty or not.
-----------------------------------------------------------------
-- [ HUDHive.Settings.AgendaPosition ]
-- Positioning:
--      1 = topLeft
--      2 = topRight
--      3 = bottomLeft
--      4 = bottomRight
-----------------------------------------------------------------

HUDHive.Settings.AgendaHideIfEmpty = false
HUDHive.Settings.AgendaPosition = 1
HUDHive.Settings.AgendaIcon = "hudhive/hh_agenda.png"
HUDHive.Settings.AgendaIconColor = Color(255, 255, 255, 255)
HUDHive.Settings.AgendaTitleTextColor = Color(255, 255, 255, 255)
HUDHive.Settings.AgendaContentTextColor = Color(255, 255, 255, 255)

-----------------------------------------------------------------
-- [ HEAD HUD ]
-----------------------------------------------------------------

HUDHive.Settings.HeadHudEnabled = true

HUDHive.Settings.HeadHudDrawDistance = 115000
HUDHive.Settings.HeadHudPositionVertical = 105
HUDHive.Settings.HeadHudShowJob = true
HUDHive.Settings.HeadHudJobTextColor = Color(255,255,255,255)
HUDHive.Settings.HeadHudPlayernameEnabled = true

HUDHive.Settings.HeadHudShowHealthNumber = true
HUDHive.Settings.HeadHudHealthBarEnabled = true
HUDHive.Settings.HeadHudHealthBarColor = Color(8, 156, 75, 255)
HUDHive.Settings.HeadHudHealthBarBGColor = Color( 25, 25, 25, 200 )
HUDHive.Settings.HeadHudHealthNumberColor = Color(255,255,255,255)

HUDHive.Settings.HeadHudArmorBarEnabled = true
HUDHive.Settings.HeadHudArmorBarBGColor = Color( 163, 135, 79, 255 )
HUDHive.Settings.HeadHudWantedTextColor = Color( 255, 125, 125, 255)
HUDHive.Settings.HeadHudGunlicenseIcon = "hudhive/hh_license.png"
HUDHive.Settings.HeadHudGunlicenseIconColor = Color( 255,255,255,255 )

HUDHive.Settings.HeadHudShowXPNumber = true
HUDHive.Settings.HeadHudXPBarEnabled = true
HUDHive.Settings.HeadHudXPNumberColor = Color(255,255,255,255)
HUDHive.Settings.HeadHudXPBarColor = Color(124, 51, 50, 190)
HUDHive.Settings.HeadHudXPBarBGColor = Color( 25, 25, 25, 200 )

HUDHive.Settings.HeadHudOrganizationsEnabled = true
HUDHive.Settings.HeadHudOrganizationsDefaultColor = Color( 255, 255, 255, 255 )

-----------------------------------------------------------------
-- Positioning. Allows you to adjust where at these items are 
-- (vertical) - everything horizontal is center aligned.
-----------------------------------------------------------------
HUDHive.Settings.HeadHudJobHPos = 20
HUDHive.Settings.HeadHudPlayernameHPos = 42
HUDHive.Settings.HeadHudNicknamesHPos = 92
HUDHive.Settings.HeadHudOrganizationsHPos = 2
HUDHive.Settings.HeadHudHealthHPos = 76
HUDHive.Settings.HeadHudXPHPos = 62
HUDHive.Settings.HeadHudArmorHPos = 90

-----------------------------------------------------------------
-- [ HEAD HUD -> USER GROUPS ]
-----------------------------------------------------------------
-- Will display the usergroup above the player's head if it is 
-- listed in the tables below.
-----------------------------------------------------------------
-- You can only use Nicknames OR UserGroups one at a time. If 
-- both are on, then neither will show. Set one to false if this
-- happens.
-----------------------------------------------------------------

HUDHive.Settings.HeadHudShowUserGroup = true

HUDHive.Settings.UserGroupTitles = {}
HUDHive.Settings.UserGroupTitles["superadmin"] = "Owner"
HUDHive.Settings.UserGroupTitles["admin"] = "Administrator"
HUDHive.Settings.UserGroupTitles["supervisor"] = "Supervisor"
HUDHive.Settings.UserGroupTitles["operator"] = "Moderator"
HUDHive.Settings.UserGroupTitles["moderator"] = "Moderator"
HUDHive.Settings.UserGroupTitles["trialmod"] = "Trial Moderator"

HUDHive.Settings.UserGroupColors = {}
HUDHive.Settings.UserGroupColors["superadmin"] = Color( 200, 51, 50, 220 )
HUDHive.Settings.UserGroupColors["admin"] = Color( 72, 112, 58, 220 )
HUDHive.Settings.UserGroupColors["supervisor"] = Color( 145, 71, 101, 220 )
HUDHive.Settings.UserGroupColors["operator"] = Color( 171, 108, 44, 220 )
HUDHive.Settings.UserGroupColors["moderator"] = Color( 171, 108, 44, 220 )
HUDHive.Settings.UserGroupColors["trialmod"] = Color( 163, 135, 79, 220 )

-----------------------------------------------------------------
-- [ HEAD HUD -> NICKNAMES ]
-----------------------------------------------------------------
-- Will display a custom nickname above the player's head if 
-- their Steam32 ID is listed in the table below.
-----------------------------------------------------------------
-- You can only use Nicknames OR UserGroups one at a time. If 
-- both are on, then neither will show. Set one to false if this
-- happens.
-----------------------------------------------------------------
-- To find the Steam32 ID of someone on steam outside your server
-- you can use the Steam ID Finder Tool:
-- http://steamidfinder.com/
-----------------------------------------------------------------

HUDHive.Settings.HeadHudShowNicknames = false

HUDHive.Settings.NicknameTitles = {}
HUDHive.Settings.NicknameTitles["NULL"] = "BOT"
HUDHive.Settings.NicknameTitles["STEAM_0:1:87804999"] = "DEVELOPER"

HUDHive.Settings.NicknameColors = {}
HUDHive.Settings.NicknameColors["NULL"] = Color( 200, 51, 50, 220 )
HUDHive.Settings.NicknameColors["STEAM_0:1:87804999"] = Color( 200, 51, 50, 220 )


-----------------------------------------------------------------
-- [ ADMINTELL NOTIFICATION ]
-----------------------------------------------------------------
-- [ HUDHive.Settings.AdminTellUseCustom ]
-- 
-- true     :   Will use the HUDHive admintell GUI.
-- false    :   Will use the default DarkRP admintell GUI
-----------------------------------------------------------------
-- [ HUDHive.Settings.AdminTellTimer ]
-- 
-- The time (in seconds) a message will stay on the screen when
-- a staff member initializes admintell.
-----------------------------------------------------------------

HUDHive.Settings.AdminTellUseCustom = true
HUDHive.Settings.AdminTellTimer = 10
HUDHive.Settings.AdminTellBoxPrimaryColor = Color(15,15,15,240)
HUDHive.Settings.AdminTellBoxSecondaryColor = Color(15,15,15,200)
HUDHive.Settings.AdminTellTitleColor = Color(255,255,255,255)
HUDHive.Settings.AdminTellMessageColor = Color(255,255,255,255)

-----------------------------------------------------------------
-- [ LOCKDOWN NOTIFICATION ]
-----------------------------------------------------------------

HUDHive.Settings.LockdownNotiEnabled = true
HUDHive.Settings.LockdownIcon = "hudhive/hh_lockdown.png"
HUDHive.Settings.LockdownBoxText = "A lockdown is active. Please return to your homes until it has been lifted."

-----------------------------------------------------------------
-- [ WANTED NOTIFICATION ]
-----------------------------------------------------------------

HUDHive.Settings.WantedNotiEnabled = true
HUDHive.Settings.WantedBoxIcon = "hudhive/hh_wanted.png"
HUDHive.Settings.WantedBoxText = "You are wanted!"
HUDHive.Settings.ArrestedIcon = "hudhive/hh_arrested.png"

-----------------------------------------------------------------
-- [ MESSAGE NOTIFICATION ]
-----------------------------------------------------------------

HUDHive.Settings.NotiBoxColorGeneric = Color(15, 15, 15, 255)
HUDHive.Settings.NotiBoxColorError = Color(124, 51, 50, 255)
HUDHive.Settings.NotiBoxColorOkay = Color(8, 156, 75, 255)
HUDHive.Settings.NotiBoxColorHint = Color(64, 105, 126, 255)
HUDHive.Settings.NotiBoxColorCleanup = Color(64, 105, 126, 255)
HUDHive.Settings.NotiBoxTextColor = Color(255,255,255,255)
HUDHive.Settings.NotiBoxRightPos = 15
HUDHive.Settings.NotiBoxHeightPos = 150

